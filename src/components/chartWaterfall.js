// CommitChart.js




import { Bar } from 'vue-chartjs'






export default {
  extends: Bar,
  mounted () {
    // Overwriting base render method with actual data.
    this.renderChart({
    // labels: ['RM', 'BOP', 'RMtotal', 'VA', 'OH & OTHER', 'Profit', 'Total Cost', 'GAP', 'Client Cost'],

  datasets: [
    {
      label: 'Closing Costs',
      data: [50],
      backgroundColor: '#e8cdd7',
      stack: 'stack 1',
    },
    {
      label: 'Purchase Price',
      data: [700],
      backgroundColor: '#d29baf',
      stack: 'stack 1',
    },
    {
      data: [200],
      waterfall: {
        dummyStack: true,
      },
      stack: 'stack 2',
    },
    {
      label: 'Opening Loan Balance',
      data: [550],
      backgroundColor: '#bb6987',
      stack: 'stack 2',
    },
    {
      label: 'Initial Cash Investment',
      data: [200],
      backgroundColor: '#a53860',
      stack: 'stack 3',
    },
  ]
   })
  }
}