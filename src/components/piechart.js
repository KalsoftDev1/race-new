// CommitChart.js
import { Pie } from 'vue-chartjs'


export default {
  props: { ['chartData']: { type: Object, required: true } },
  extends: Pie,
  mounted() {
    // Overwriting base render method with actual data.
    console.log("PieChart", this.chartData)

    this.renderChart({
      labels: ['IRON', 'BOP', 'RMtotal', 'VA', 'OH & OTHER', 'SAND'],
      datasets: [
        {
          label: 'VA RM Breakdown',
          backgroundColor: '#1c4870',
          data: [40, 20, 12, 39, 10, 40,],
          options: {

            layout: {
              padding: {
                left: 50,
                right: 0,
                top: 0,
                bottom: 0
              },
              width: "200px",
              height: "200px"
            }
          }
        }
      ]
    })
  },
  watch: {

    chartData() {

      let raceOutPutChartData = this.chartData;
      console.log("Watch", raceOutPutChartData)

      let c_fe = [raceOutPutChartData.IRON, raceOutPutChartData.Glass,raceOutPutChartData.HPDC_Material], 
      c_fe = c_fe.filter(c_fe => c_fe)[0];
      let c_cs = raceOutPutChartData.CoreSand;

      let c_gs = raceOutPutChartData.GreenSand;


      let c_Mc = raceOutPutChartData.Machine;
      let c_lab = raceOutPutChartData.Labour;
      let c_Bsetp = raceOutPutChartData.BatchSetup;

      let allLabels = [[Object.keys($chartData[0])[9], Object.keys($chartData[0])[10], Object.keys($chartData[0])[11]], ['Machine', 'Labour', 'BatchSetup']];
      
      this.$set(this.data, 0, trace1);
    }
  }
}