
const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      { 
        path: '', 
        component: () => import('pages/login.vue') ,
        name:'login'
      },
      { 
        path: '/dashboard', 
        component: () => import('pages/PageDashboard.vue') ,
        name:'Dashboard'
      },
      { 
        path: '/PartDetail', 
        component: () => import('pages/PageProject.vue') ,
        name:'PartDetail'
      },
      { 
        path: '/ProjectDetail', 
        component: () => import('pages/ProjectDetail.vue') ,
        name:'Project-Detail',
        props:true
      },
      { 
        path: '/Analysis', 
        component: () => import('pages/pageAnalysis.vue') ,
        name:'Chart',
        props:true
      },
      { 
        path: '/AnalysisCopy', 
        component: () => import('pages/pageAnalysis copy.vue') ,
        name:'WaterFall-Chart',
        props:true
      },
      { 
        path: '/AnalysisUploadBOM', 
        component: () => import('pages/pageAnalysisUploadBOM.vue') ,
        name:'excel-Upload',
        props:true
      },
      { 
        path: '/AnalysisExcel', 
        component: () => import('pages/pageExcelUI.vue') ,
        name:'excel-UI',
        props:true
      },
      { 
        path: '/AnalysisTreeTable', 
        component: () => import('pages/pageTreetableDrag.vue') ,
        name:'treeTable-UI',
        props:true
      },
      { 
        path: '/AnalysisParato', 
        component: () => import('pages/pageAnalysisParatoChart.vue') 
      },
      { 
        path: '/primaryV', 
        component: () => import('pages/pagePrimaryV.vue') ,
        name:'primary-variable',
        props:true
      },
      { 
        path: '/controlV', 
        component: () => import('pages/pageControlV.vue') ,
        name:'control-variable',
        props:true
      },
      //VTL
      {
        path: '/homepage',
        component: () => import('pages/login/homepage.vue'),
        name: 'homepage'
      },
      {
        path: '/compareModule',
        component: () => import('pages/login/compareModule.vue'),
        name: 'compareModule'
      },
      {
        path: '/designModule',
        component: () => import('pages/login/designModule.vue'),
        name: 'designModule'
      },
    
    
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
