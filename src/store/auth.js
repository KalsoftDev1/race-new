import api from '../api/auth-outh'
import qs from 'qs';
import Router from 'vue-router'


const state = {
  token: window.localStorage.getItem('imgur_token')
};

const getters = {
  //!! converinto boolen
  isLoggedIn: state => !!state.token
};

const actions = {
  login: () => {
    api.login();
  
  },
  finalizeLogin({ commit }, hash) {
    //const query = qs.parse(hash.replace('#', ''));

    commit('setToken', 'query.access_token');
    window.localStorage.setItem('imgur_token', 'query.access_token');
    this.$router.push('/dashboard');
  },
  logout: ({ commit }) => {
    commit('setToken', null);
    window.localStorage.removeItem('imgur_token');
   // this.$router.push('/'); WHY NOT WORKING??
  }
};

const mutations = {
  setToken: (state, token) => {
    state.token = token;
  }
};

export  const auth = {
  state,
  getters,
  actions,
  mutations
};