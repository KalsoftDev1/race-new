import axios from "axios";

const state = {

   headerName: "",
   appHeader:false,
   appLogout:false,
   userId:null,
   

}

const mutations = {



   UpdateHeaderName(state, payload) {
      state.headerName = payload;

   },
   UpdateAppHeaderName(state, payload) {
      state.appHeader = payload;

   },
   
   UpdateApplogout(state, payload) {
      state.appLogout = payload;

   },
   UpdateUserId(state, payload) {
      state.userId = payload;

   },

}

const actions = {

   //async server   

   // getProcPrimV({ commit }) {
   //    //call Api

   //    commit('setProcPrimv', response)
   // },

   // setAssemblyName({ commit, payload }) {
   //    //call Api

   //    commit('SETASSEMBLYNAME', payload)
   // },

   // setSelProcProcMatId({ commit, payload }) {
   //    //call Api

   //    commit('SetMatId', payload)
   // },


   updateHeaderName( context, payload ) {
      //call Api
      
      context.commit('UpdateHeaderName', payload)
     
   },
   updateAppHeaderName( context, payload ) {
      //call Api
      
      context.commit('UpdateAppHeaderName', payload)
     
   },
   updateApplogout( context, payload ) {
      //call Api
      
      context.commit('UpdateApplogout', payload)
     
   },
   
   updateUserId( context, payload ) {
      //call Api
      
      context.commit('UpdateUserId', payload)
     
   },

}

const getters = {

   //manupalute and send to components
   getHeaderName: state => {
      return state.headerName
   },
   getAppHeader: state => {
      return state.appHeader
   },
   getAppLogOut: state => {
      return state.appLogout
   },
   getUserId: state => {
      return state.userId
   }
 





}

export const appStateStore = {
   namespaced: true,
   state,
   mutations,
   actions,
   getters
}