
const state = {




    model: {

        modelName: [],
        modelSpecs:[],
        modelSpecsData: [],

    }

}





const mutations = {



    UpdateModelSpces(state, payload) {
        state.model.modelSpecs = payload;
        // Object.assign(state.selectedProfile, payload.updates)
    },
    
    UpdateModelSpcesData(state, payload) {
        state.model.modelSpecsData = payload;
        // Object.assign(state.selectedProfile, payload.updates)
    },



}

const actions = {

    //async server 

    updateModelSpces(context, payload) {

        context.commit('UpdateModelSpces', payload)
    },

    updateModelSpcesData(context, payload) {

        context.commit('UpdateModelSpcesData', payload)
    }




}

const getters = {

    //manupalute and send to components
    getSelectedModelsData: state => {
        return state.model.modelSpecsData
    },
    getSelectedModels: state => {
        return state.model.modelSpecs
    }


}

export const VTL = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}