import Vue from 'vue'
import Vuex from 'vuex'



 import {mfgstore1 } from '../store/store-manufacturingProfile'
 import { processStore } from '../store/store-process'
 import { auth } from '../store/auth'
 import { project } from '../store/project-store'
 import { appStateStore } from '../store/store-appstate'
 import { VTL } from '../store/store-VTL'
 




Vue.use(Vuex)
//Vue.use(FeathersVuex)
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 * 
 */
// const requireModule = require.context(
//   // The path where the service modules live
//   './services',
//   // Whether to look in subfolders
//   false,
//   // Only include .js files (prevents duplicate imports`)
//   /.js$/
// )
// const servicePlugins = requireModule
//   .keys()
//   .map(modulePath => requireModule(modulePath).default)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
          mfgstore1,
          processStore,
          auth,
          project,
          appStateStore,
          VTL

    },
   // plugins: [...servicePlugins]
  })


  return Store
}
